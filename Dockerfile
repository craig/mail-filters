FROM golang:latest

RUN go get -u github.com/mbrt/gmailctl/cmd/gmailctl && \
    go install github.com/mbrt/gmailctl/cmd/gmailctl@latest
